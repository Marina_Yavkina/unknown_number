 using interfaces;
 //SRP, OPEN-CLOSED PRINCIPLES
public class Validation 
    {

        private IValidationMessage _validationMessage ;
        public Validation(IValidationMessage validationMessage)
        {
            _validationMessage = validationMessage;
        }
        public ValidationResult Validate()
        {
            return _validationMessage.ValidationMessageReturn();
        }
        
    }