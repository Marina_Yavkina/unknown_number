using System;
using interfaces;
public class InterfaceGame   
{
    //Dependency Injversion Principle
    private IGeneratorNumber _generatorNumber;
    private IComparatorNumber _comparatorNumber;

        public InterfaceGame ( IGeneratorNumber generatorNumber, IComparatorNumber comparatorNumber)
        {
            _generatorNumber = generatorNumber;
            _comparatorNumber = comparatorNumber;
        }

        public string StartGame(int ways, (int min, int max) t )
        {
           
            int generetedNumber =  _generatorNumber.GenerateNumerWhithRange((t.min,t.max));
            ValidationResultReturn valid_res = new ValidationResultReturn (_comparatorNumber);
            return valid_res.ReturnFinishMessage(ways,generetedNumber);
            
        }


}