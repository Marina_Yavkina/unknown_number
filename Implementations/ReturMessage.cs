using interfaces;
//OCP
public class ReturnRight : IValidationMessage
{
    public ValidationResult ValidationMessageReturn()
    {
       return new(true, "you are right!");
    }
}

public class ReturnBigger : IValidationMessage
{
    public ValidationResult ValidationMessageReturn()
    {
       return new(false, "original number is bigger!");
    }
}

public class ReturnLess : IValidationMessage
{
    public ValidationResult ValidationMessageReturn()
    {
       return  new(false, "original number is less!");
    }
}