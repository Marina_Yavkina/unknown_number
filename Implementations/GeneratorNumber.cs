using interfaces;
//SRP
public class GeneratorOriginNumber : IGeneratorNumber
{
    public int GenerateNumerWhithRange((int min, int max) t){
        var random = new Random();
        return random.Next(t.min, t.max);
    }
}