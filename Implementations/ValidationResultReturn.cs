using System;
using System.Data;
using interfaces;
public class ValidationResultReturn   
{
    //Dependency Injversion Principle
    private IComparatorNumber _comparatorNumber;

        public ValidationResultReturn (IComparatorNumber comparatorNumber)
        {
            _comparatorNumber = comparatorNumber;
        }

        public string ReturnFinishMessage(int ways, int generetedNumber )
        {
            int res = -1;
            Console.WriteLine( "Какое это число?");
            int guesseNumber = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < ways; i++)
            {
                res = _comparatorNumber.CompareNumers((guesseNumber,generetedNumber));

                IValidationMessage vm  =  new ReturnRight();;
                if (res == 2 )
                {
                    vm = new ReturnRight();
                }
                else if (res == 0)
                {
                    vm = new ReturnLess();
                } 
                else if (res == 1)
                {
                    vm = new ReturnBigger();
                } 
                Validation val = new Validation(vm);   
                ValidationResult ret = val.Validate();
                    
                if (ret.IsValid )
                {
                    return ret.Message;
                }
                else if (i == ways-1)
                { 
                    return string.Concat(ret.Message , " Game over!");
                }
                else{
                    Console.WriteLine(ret.Message); 
                    Console.WriteLine( "Попытайтесь еще");
                    guesseNumber  = Convert.ToInt32(Console.ReadLine());
                   
                }                
            }
            return "";
        }


}