namespace interfaces
{
    public interface IValidation
    {
        public ValidationResult Validate(int number);
        
    }
}