public enum CompareResults
{
    Bigger,
    Less,
    Equal,
    Fail
}