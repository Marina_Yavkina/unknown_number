﻿// See https://aka.ms/new-console-template for more information


Console.WriteLine( "Игра Отгадай число.");
Console.WriteLine( "Сколько будет попыток?");
int ways = Convert.ToInt32(Console.ReadLine());
Console.WriteLine( "Задайте максимальное значение числа");
int max = Convert.ToInt32(Console.ReadLine());
Console.WriteLine( "Задайте минимальное значение числа");
int min = Convert.ToInt32(Console.ReadLine());
GeneratorOriginNumber gn = new GeneratorOriginNumber();
ComparatorNumber cn = new ComparatorNumber();
InterfaceGame  interfaceGame = new InterfaceGame(gn,cn);

var res = interfaceGame.StartGame(ways,(min,max));
Console.WriteLine(res);
